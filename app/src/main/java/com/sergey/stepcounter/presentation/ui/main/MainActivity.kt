package com.sergey.stepcounter.presentation.ui.main

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.sergey.stepcounter.R
import com.sergey.stepcounter.presentation.extensions.inTransaction
import com.sergey.stepcounter.presentation.ui.base.BaseActivity
import com.sergey.stepcounter.presentation.ui.main.step.StepCounterFragment

class MainActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)

        supportFragmentManager.apply {
            if (findFragmentByTag(STEP_COUNTER_FRAGMENT_TAG) == null) {
                inTransaction {
                    add(R.id.container, StepCounterFragment.newInstance(), STEP_COUNTER_FRAGMENT_TAG)
                }
            }
        }
    }

    companion object {
        const val STEP_COUNTER_FRAGMENT_TAG = "STEP_COUNTER_FRAGMENT_TAG"

        fun makeLaunchIntent(context: Context) = Intent(context, MainActivity::class.java)
    }
}
