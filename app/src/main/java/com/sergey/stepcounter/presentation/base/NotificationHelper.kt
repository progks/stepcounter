package com.sergey.stepcounter.presentation.base

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.support.v4.app.NotificationCompat
import android.support.v4.app.NotificationManagerCompat
import com.sergey.stepcounter.R
import com.sergey.stepcounter.presentation.extensions.getNotificationManager
import com.sergey.stepcounter.presentation.extensions.isOreo
import com.sergey.stepcounter.presentation.ui.main.MainActivity

class NotificationHelper(
    private val mContext: Context
) {

    fun createNotificationChannel() {
        if (isOreo()) {
            val name = mContext.getString(R.string.notifcaiton_channel_name)
            val importance = NotificationManager.IMPORTANCE_LOW
            val channel = NotificationChannel(CHANNEL_ID, name, importance)
            mContext.getNotificationManager().createNotificationChannel(channel)
        }
    }


    fun createStepNotification(count: Int) : Notification  =
        NotificationCompat.Builder(mContext, CHANNEL_ID)
            .setSmallIcon(R.mipmap.ic_launcher)
            .setContentTitle(mContext.getString(R.string.app_name))
            .setContentText(mContext.getString(R.string.step_counter_steps_count, count))
            .setStyle(NotificationCompat.BigTextStyle())
            .setPriority(NotificationCompat.PRIORITY_DEFAULT)
            .setChannelId(CHANNEL_ID)
            .setContentIntent(PendingIntent.getActivity(mContext, INTENT_RC, MainActivity.makeLaunchIntent(mContext), PendingIntent.FLAG_UPDATE_CURRENT))
            .build()


    fun updateNotification(count: Int) {
        createStepNotification(count).let {
            NotificationManagerCompat.from(mContext).notify(NOTIFICATION_ID, it)
        }
    }

    companion object {
        private const val CHANNEL_ID = "notifications"
        const val NOTIFICATION_ID = 100
        private const val INTENT_RC = 1000
    }
}