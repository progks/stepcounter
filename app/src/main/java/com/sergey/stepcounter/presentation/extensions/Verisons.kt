package com.sergey.stepcounter.presentation.extensions

import android.os.Build

fun isOreo() = Build.VERSION.SDK_INT >= Build.VERSION_CODES.O