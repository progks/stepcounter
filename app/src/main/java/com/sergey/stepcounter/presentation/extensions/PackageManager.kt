package com.sergey.stepcounter.presentation.extensions

import android.content.pm.PackageManager

fun PackageManager.isAccelerometerAvailable() : Boolean =
    hasSystemFeature(PackageManager.FEATURE_SENSOR_ACCELEROMETER)