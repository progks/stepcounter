package com.sergey.stepcounter.presentation.base

class StepCounterHelper {

    private val previousLinearAcceleration = doubleArrayOf(0.0, 0.0, 0.0)
    private val gravity = doubleArrayOf(0.0, 0.0, 0.0)

    private var previousTimeNs = 0L

    fun isStep(timeNS: Long, x: Float, y: Float, z: Float) : Boolean {

        val alpha = 0.8

        gravity[0] = alpha * gravity[0] + (1 - alpha) * x
        gravity[1] = alpha * gravity[1] + (1 - alpha) * y
        gravity[2] = alpha * gravity[2] + (1 - alpha) * z

        val linearAcceleration = doubleArrayOf(0.0, 0.0, 0.0)

        linearAcceleration[0] = x - gravity[0]
        linearAcceleration[1] = y - gravity[1]
        linearAcceleration[2] = z - gravity[2]


        val result = if ((isStep(linearAcceleration[0], previousLinearAcceleration[0]) ||
            isStep(linearAcceleration[1], previousLinearAcceleration[1]) ||
            isStep(linearAcceleration[2], previousLinearAcceleration[2]))
            && timeNS - previousTimeNs > TIME_THRESHOLD_NS) {
            previousTimeNs = timeNS
            true
        } else {
            false
        }

        previousLinearAcceleration[0] = linearAcceleration[0]
        previousLinearAcceleration[1] = linearAcceleration[1]
        previousLinearAcceleration[2] = linearAcceleration[2]

        return result
    }


    companion object {
        private const val ACCELERATION_THRESHOLD = 3
        private const val TIME_THRESHOLD_NS = 300000000

        private fun getDiffAbs(value1: Double, value2: Double) = Math.abs(value1 - value2)

        private fun isStep(value1: Double, value2: Double) =
            getDiffAbs(value1, value2) > ACCELERATION_THRESHOLD
    }


}