package com.sergey.stepcounter.presentation.ui.main.step

import android.support.annotation.LayoutRes
import com.arellomobile.mvp.presenter.InjectPresenter
import com.sergey.stepcounter.R
import com.sergey.stepcounter.presentation.base.StepService
import com.sergey.stepcounter.presentation.extensions.gone
import com.sergey.stepcounter.presentation.extensions.isAccelerometerAvailable
import com.sergey.stepcounter.presentation.extensions.visible
import com.sergey.stepcounter.presentation.ui.base.BaseMvpFragment
import kotlinx.android.synthetic.main.fragment_step_counter.*

class StepCounterFragment : BaseMvpFragment(), StepCounterView {

    @InjectPresenter
    lateinit var mPresenter: StepCounterPresenter


    @LayoutRes
    override fun getLayoutRes() = R.layout.fragment_step_counter

    override fun onStart() {
        super.onStart()
        mPresenter.onAccelerometerAvailable(context?.packageManager?.isAccelerometerAvailable() ?: false)
    }

    override fun showStepsInfo() {
        tv_steps_count.visible()
        tv_steps.visible()
    }

    override fun hideStepsInfo() {
        tv_steps_count.gone()
        tv_steps.gone()
    }

    override fun showSensorNotAvailable() {
        tv_not_available.visible()
    }

    override fun hideSensorNotAvailable() {
        tv_not_available.gone()
    }

    override fun setSteps(count: Int) {
        tv_steps_count.text = count.toString()
    }

    override fun startCalculateSteps() {
        context?.apply {
            startService(StepService.makeLaunchIntent(this))
        }
    }

    override fun stopCalculateSteps() {
        context?.apply {
            stopService(StepService.makeLaunchIntent(this))
        }
    }


    companion object {

        fun newInstance() = StepCounterFragment()
    }


}