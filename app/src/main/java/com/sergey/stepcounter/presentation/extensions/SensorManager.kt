package com.sergey.stepcounter.presentation.extensions

import android.hardware.Sensor
import android.hardware.SensorManager


fun SensorManager.getAccelerometer(): Sensor = getDefaultSensor(Sensor.TYPE_ACCELEROMETER)