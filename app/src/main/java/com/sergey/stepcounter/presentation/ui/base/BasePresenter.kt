package com.sergey.stepcounter.presentation.ui.base

import com.arellomobile.mvp.MvpPresenter

open class BasePresenter<T: BaseMvpView> : MvpPresenter<T>()