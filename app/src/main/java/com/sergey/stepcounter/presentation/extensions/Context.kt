package com.sergey.stepcounter.presentation.extensions

import android.app.NotificationManager
import android.content.Context
import android.hardware.SensorManager

fun Context.getSensorManager() : SensorManager {
    return getSystemService(Context.SENSOR_SERVICE) as SensorManager
}

fun Context.getNotificationManager() : NotificationManager {
    return getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
}