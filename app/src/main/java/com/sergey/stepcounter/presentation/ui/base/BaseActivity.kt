package com.sergey.stepcounter.presentation.ui.base

import android.support.v7.app.AppCompatActivity

abstract class BaseActivity : AppCompatActivity()