package com.sergey.stepcounter.presentation.base

import android.content.Context
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import com.sergey.stepcounter.presentation.extensions.getAccelerometer
import com.sergey.stepcounter.presentation.extensions.getSensorManager

class AccelerometerSensorManager(
    private val context: Context
) {

    private val mSensorEventListener = object : SensorEventListener {
        override fun onAccuracyChanged(sensor: Sensor?, accuracy: Int) {

        }

        override fun onSensorChanged(event: SensorEvent) {
            val values = event.values

            mListener?.onChanged(event.timestamp, values[0], values[1], values[2])
        }

    }

    private var mIsRegistered: Boolean = false

    private var mListener : Listener? = null


    fun startListening(listener: Listener) {
        if (mIsRegistered) {
            return
        }

        mIsRegistered = true

        mListener = listener

        context.getSensorManager().apply {
            registerListener(mSensorEventListener, getAccelerometer(), SensorManager.SENSOR_DELAY_NORMAL)
        }
    }

    fun stopListening() {
        if (!mIsRegistered) {
            return
        }

        mIsRegistered = false

        context.getSensorManager().unregisterListener(mSensorEventListener)

        mListener = null
    }


    interface Listener {
        fun onChanged(timeNs: Long, x: Float, y: Float, z: Float)
    }
}