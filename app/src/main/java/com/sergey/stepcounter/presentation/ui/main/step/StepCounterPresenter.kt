package com.sergey.stepcounter.presentation.ui.main.step

import com.arellomobile.mvp.InjectViewState
import com.sergey.stepcounter.data.StepStorage
import com.sergey.stepcounter.presentation.ui.base.BasePresenter

@InjectViewState
class StepCounterPresenter : BasePresenter<StepCounterView>() {

    private val mStepChangedListener = object : StepStorage.Listener {
        override fun onStepChanged(count: Int) {
            viewState.setSteps(count)
        }
    }

    fun onAccelerometerAvailable(isAvailable: Boolean) {
        if (isAvailable) {
            setAccelerometerAvailable()
            viewState.startCalculateSteps()
            viewState.setSteps(StepStorage.steps)
            StepStorage.addListener(mStepChangedListener)
        } else {
            viewState.stopCalculateSteps()
            setAccelerometerNotAvailable()
        }
    }

    private fun setAccelerometerAvailable() {
        viewState.hideSensorNotAvailable()
        viewState.showStepsInfo()
    }

    private fun setAccelerometerNotAvailable() {
        viewState.hideStepsInfo()
        viewState.showSensorNotAvailable()
    }
}