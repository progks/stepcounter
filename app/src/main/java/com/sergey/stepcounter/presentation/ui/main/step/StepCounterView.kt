package com.sergey.stepcounter.presentation.ui.main.step

import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.sergey.stepcounter.presentation.ui.base.BaseMvpView

interface StepCounterView : BaseMvpView {

    fun showStepsInfo()

    fun hideStepsInfo()

    fun showSensorNotAvailable()

    fun hideSensorNotAvailable()

    @StateStrategyType(AddToEndSingleStrategy::class)
    fun setSteps(count: Int)

    @StateStrategyType(AddToEndSingleStrategy::class)
    fun startCalculateSteps()

    @StateStrategyType(SkipStrategy::class)
    fun stopCalculateSteps()

}