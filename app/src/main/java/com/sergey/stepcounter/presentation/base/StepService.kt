package com.sergey.stepcounter.presentation.base

import android.app.Service
import android.content.Context
import android.content.Intent
import com.sergey.stepcounter.data.StepStorage

class StepService : Service() {

    private val mSensorManager: AccelerometerSensorManager = AccelerometerSensorManager(this)

    private val mStepCounterHelper = StepCounterHelper()

    private val mNotificationHelper = NotificationHelper(this)

    private val mAccelerometerListener = object : AccelerometerSensorManager.Listener {
        override fun onChanged(timeNs: Long, x: Float, y: Float, z: Float){

            if (mStepCounterHelper.isStep(timeNs, x, y, z)) {
                StepStorage.steps++
            }
        }
    }

    private val mStepChangeListener = object : StepStorage.Listener {
        override fun onStepChanged(count: Int) {
            mNotificationHelper.updateNotification(count)
        }
    }

    override fun onCreate() {
        super.onCreate()
        mNotificationHelper.createNotificationChannel()
        StepStorage.addListener(mStepChangeListener)
        mSensorManager.startListening(mAccelerometerListener)

        startForeground(NotificationHelper.NOTIFICATION_ID, mNotificationHelper.createStepNotification(StepStorage.steps))
    }

    override fun onDestroy() {
        super.onDestroy()
        mSensorManager.stopListening()
    }


    override fun onBind(intent: Intent) = null


    companion object {

        fun makeLaunchIntent(context: Context) = Intent(context, StepService::class.java)
    }

}
