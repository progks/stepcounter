package com.sergey.stepcounter.data

import java.lang.ref.WeakReference
import kotlin.properties.Delegates

object StepStorage {

    private val mChangeListener = mutableListOf<WeakReference<Listener>>()

    private var mInternalSteps: Int by Delegates.observable(0, onChange = { _, _, steps ->
        onStepChanged(steps)
    })

    var steps: Int
        get() = mInternalSteps
        set(value) {
            mInternalSteps = value
        }

    fun addListener(listener: Listener) {
        mChangeListener.add(WeakReference(listener))
    }

    private fun onStepChanged(steps: Int) {
        val iterator = mChangeListener.iterator()
        while (iterator.hasNext()) {
            iterator.next().get()?.onStepChanged(steps) ?: iterator.remove()
        }
    }


    interface Listener {
        fun onStepChanged(count: Int)
    }
}